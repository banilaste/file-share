package io.github.banilaste.fileshare.client.caching

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class CacheEntryTest {
    private lateinit var entry: CacheEntry

    private fun assertResult(expected: String, result: CacheReadResult?) {
        if (result == null) {
            return fail("result not received")
        }

        assertEquals(expected, result.data.toString(Charsets.US_ASCII))
    }

    private fun assertRead(expected: String, offset: Long) {
        assertResult(expected, entry.read(offset, expected.toByteArray().size))
    }

    private fun put(offset: Long, content: String) {
        entry.put(offset, content.toByteArray())
    }

    @BeforeEach
    fun init() {
        entry = CacheEntry()
    }

    @Test
    fun testCaching() {
        put(0, "hello world")
        assertRead("hello world", 0)
        assertRead("llo world", 2)
        assertRead("llo worl", 2)

        put(6, "you")
        assertRead("hello you", 0)
    }

    @Test
    fun testGapFilling() {
        put(0, "hi ")
        put(8, "jacob")
        put(3, "I am ")

        assertRead("hi I am jacob", 0)
    }

    @Test
    fun testComplexGapFilling() {
        put(0, "a")
        put(11, "abcd")
        put(16, " !")
        put(5, "cc")
        put(40, "hoho")

        assertNull(entry.read(0, 2))

        put(1, "wayoflocomotion")

        assertRead("awayoflocomotion !", 0)
        assertRead("hoho", 40)
    }

    @Test
    fun testOverflow() {
        put(1, "hi")

        assertNull(entry.read(0, 3))
        assertNull(entry.read(-1, 3))
        assertNull(entry.read(4, 1))
    }

    @Test
    fun testCacheClearing() {
        put(3, "hi jacob")
        assertRead("hi jac", 3)

        entry.wipe()

        assertNull(entry.read(4, 2))
    }
}