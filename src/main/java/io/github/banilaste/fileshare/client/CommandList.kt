package io.github.banilaste.fileshare.client

import io.github.banilaste.duck.generated.FileListener
import io.github.banilaste.duck.util.LOGGER
import io.github.banilaste.fileshare.common.*

/**
 * Map of all commands (transformed into a map in the last line)
 */
val COMMANDS = listOf(
    /**
     * Read method
     */
    CommandLineMethod(
        "read",
        "read a file from given offset if provided",
        listOf(FILE_NAME, OFFSET.orEmpty(), SIZE.orEmpty())
    ) { parts, fileManager ->
        when (parts.size) {
            2 -> fileManager.read(parts[1], 0, -1)
            3 -> fileManager.read(parts[1], parts[2].toLong(), -1)
            4 -> fileManager.read(parts[1], parts[2].toLong(), parts[3].toInt())
            else -> throw CommandExecutionException("invalid argument size")
        }.toString(Charsets.US_ASCII)
    },
    /**
     * Write method
     */
    CommandLineMethod(
        "write",
        "write content in a file from a given offset, create file is it does exists",
        listOf(FILE_NAME, OFFSET.orEmpty())
    ) { parts, fileManager ->
        val name = parts[1]
        val offset = if (parts.size == 3) parts[2].toLong() else 0

        print("content to be written: ")
        val content = readLine()!!

        fileManager.write(name, offset, content.toByteArray(Charsets.US_ASCII))

        null
    },
    /**
     * Insert method
     */
    CommandLineMethod(
        "insert",
        "insert content in a file from a given offset",
        listOf(FILE_NAME, OFFSET.orEmpty())
    ) { parts, fileManager ->
        val name = parts[1]
        val offset = if (parts.size == 3) parts[2].toLong() else 0

        print("content to be written: ")
        val content = readLine()!!

        fileManager.insert(name, offset, content.toByteArray(Charsets.US_ASCII))

        null
    },

    /**
     * List file method
     */
    CommandLineMethod("ls", "list content of a directory", listOf(FILE_NAME.orEmpty())) { parts, fileManager ->
        fileManager.ls(if (parts.size > 1) parts[1] else "/").joinToString()
    },

    /**
     * Create directory method
     */
    CommandLineMethod("mkdir", "create recursively a directory", listOf(FILE_NAME)) { parts, fileManager ->
        fileManager.mkdir(parts[1])

        null
    },

    /**
     * Watch file method
     */
    CommandLineMethod("watch", "watch file content for a given ms duration", listOf(FILE_NAME, DURATION)) { parts, fileManager ->
        fileManager.watch(parts[1], parts[2].toLong(), FileListenerImpl.get())

        null
    },
    /**
     * Size of
     */
    CommandLineMethod("sizeof", "returns the size of a file in bytes", listOf(FILE_NAME)) { parts,fm ->
        fm.metadata(parts[1]).size.toString()
    },
    /**
     * Encrypt
     */
    CommandLineMethod("crypt", "crypt file using a vigenere like algorithm (really secure !)", listOf(FILE_NAME, CRYPT_KEY)) { parts,fm ->
        fm.crypt(parts[1], parts[2], false)
        null
    },
    /**
     * Size of
     */
    CommandLineMethod("decrypt", "decrypt file using a vigenere like algorithm", listOf(FILE_NAME, CRYPT_KEY)) { parts,fm ->
        fm.crypt(parts[1], parts[2], true)
        null
    },
    /**
     * Delete file method
     */
    CommandLineMethod("delete", "delete file", listOf(FILE_NAME)) { parts, fileManager ->
        fileManager.delete(parts[1])

        null
    }
).map { it.name to it }.toMap()