package io.github.banilaste.fileshare.client.caching

import io.github.banilaste.duck.RemoteObjectIdentifier
import io.github.banilaste.duck.generated.FileManagerStub
import io.github.banilaste.duck.generated.Metadata
import io.github.banilaste.duck.generated.Modification
import io.github.banilaste.fileshare.common.vigenere

/**
 * Extension of the default stub for file manager, adding cache capability
 */
class CachedFileManagerStub(identifier: RemoteObjectIdentifier) : FileManagerStub(identifier) {
    override fun read(path: String, offset: Long, size: Int): ByteArray {
        // Try to read cache
        var result = validReadingOf(
            path, offset, size
        )

        // or read from remote and cache
        if (result == null) {
            result = super.read(path, offset, size)
            CacheEntry.get(path).let {
                if (!it.initialized) {
                    val metadata = this.metadata(path)
                    it.lastModificationTime = metadata.modificationTime
                    it.fileSize = metadata.size
                }

                it.put(offset, result)
            }
        }

        return result
    }

    // The following require the cache to be emptied
    override fun crypt(path: String, key: String, reverse: Boolean): Modification {
        return ensureCache(path, super.crypt(path, key, reverse)) {
            it.data?.forEachFollowing { chunk ->
                chunk.data = vigenere(chunk.data, key, reverse)
            }
        }
    }

    override fun write(path: String, offset: Long, content: ByteArray): Modification {
        return ensureCache(path, super.write(path, offset, content)) {
            // Simple operation, overwrite pre-existing content
            it.put(offset, content)

            if (it.fileSize < offset + content.size) {
                it.fileSize = offset + content.size
            }
        }
    }

    override fun insert(path: String, offset: Long, content: ByteArray): Modification {
        // Insert and update cache accordingly
        return ensureCache(path, super.insert(path, offset, content)) {
            // Find first item involved
            var chunk = it.data?.locate(offset)

            if (chunk != null) {
                // If we insert in the middle of the chunk, we split the chunk
                if (chunk.offset <= offset) {
                    // Update data
                    val rightChunk = CachedChunk(offset, chunk.data.sliceArray(IntRange(offset.toInt(), chunk.data.size - 1)))
                    chunk.data = chunk.data.sliceArray(IntRange(0, (offset - 1).toInt()))

                    // Update neighbours
                    rightChunk.next = chunk.next
                    chunk.next = rightChunk

                    // Set current chunk to the one on the right to have it's offset moved afterwards
                    chunk = rightChunk
                }

                // Update offset of following chunks
                chunk.forEachFollowing { ch -> ch.offset += content.size }
            }

            // The content after the offset was moved, can safely insert the content at the freed location
            it.put(offset, content)

            // Update size
            it.fileSize += content.size
        }
    }

    override fun delete(path: String) {
        super.delete(path)

        // No need further check for modification
        CacheEntry.remove(path)
    }

    /**
     * Retrieve up to date metadata, either cached or remote.
     *
     * Ensure the current metadata are up to date
     */
    override fun metadata(path: String): Metadata {
        CacheEntry.getOrNull(path)?.let {
            if (it.initialized && !it.expired) {
                return Metadata(it.lastModificationTime, it.fileSize)
            }
        }

        val remote = super.metadata(path)
        val entry = CacheEntry.get(path)
        if (remote.modificationTime != entry.lastModificationTime) {
            entry.wipe()
            entry.lastModificationTime = remote.modificationTime
            entry.fileSize = remote.size
        } else {
            entry.refreshed()
        }

        return remote
    }

    /**
     * Ensure the cache is still valid for the given file, if so, apply operation on the cache
     */
    private fun ensureCache(path: String, modificationTimes: Modification, operation: (buffer: CacheEntry) -> Unit): Modification {
        val (previousModification, currentModificationTime, finalSize) = modificationTimes

        val entry = CacheEntry.get(path)

        // Cache was modified between operation and last check
        if (entry.lastModificationTime != previousModification && entry.initialized) {
            entry.wipe()
        } else {
            // Otherwise apply the operation on the chunk
            operation(entry)
            entry.refreshed()
        }

        // Update modification according to remote and sync size
        entry.lastModificationTime = currentModificationTime
        entry.fileSize = finalSize

        return modificationTimes
    }

    private fun validReadingOf(path: String, offset: Long, size: Int): ByteArray? {
        return CacheEntry.getOrNull(path)?.let { entry ->
            if (size == -1) { // Remainder of the file
                // If there is a possibility that the data is int the cache
                entry.readingLengthAfter(offset)?.let {
                    // Call on metadata ensure cache is up to date
                    val remoteSize = this.metadata(path).size

                    // If the cache has the capacity to read this
                    if (remoteSize - offset == it) {
                        entry.read(offset, it.toInt())?.data
                    } else {
                        null
                    }
                }
            } else { // given size
                if (entry.expired) {
                    // Call on metadata will ensure validity
                    this.metadata(path)
                }

                entry.read(offset, size)?.data
            }
        }
    }
}