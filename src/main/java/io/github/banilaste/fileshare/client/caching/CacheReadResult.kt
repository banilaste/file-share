package io.github.banilaste.fileshare.client.caching

data class CacheReadResult(val data: ByteArray, val cacheEntry: CacheEntry)