package io.github.banilaste.fileshare.client.caching

import io.github.banilaste.fileshare.common.CleanPath
import io.github.banilaste.fileshare.common.FilesProperties

class CacheEntry {
    private var expiration: Long = 0
    var data: CachedChunk? = null
    var lastModificationTime = 0L
    var fileSize = -1L

    val expired get() = expiration < System.currentTimeMillis()
    val initialized get() = lastModificationTime != 0L

    fun read(readOffset: Long, readSize: Int): CacheReadResult? {
        return data?.read(readOffset, readSize)?.let {
            CacheReadResult(it, this)
        }

    }

    /**
     * Return the amount of data the cache has stored after the following index, or null
     */
    fun readingLengthAfter(readOffset: Long): Long? {
        var chunk = data
        while (chunk != null && readOffset > chunk.rightOffset) {
            chunk = chunk.next
        }

        // Can only read the remainder if the first chunk who can handle the reading is also the last
        return if (chunk != null && chunk.next == null && chunk.offset <= readOffset)
            chunk.rightOffset - readOffset
        else
            null
    }

    fun put(offset: Long, bytes: ByteArray) {
        data = CachedChunk(
            offset,
            bytes
        ).mergeInto(data)
    }

    fun refreshed() {
        this.expiration = System.currentTimeMillis() + FilesProperties.cacheTimeout
    }

    fun wipe() {
        data = null
        refreshed()
    }

    companion object {
        private val files = HashMap<CleanPath, CacheEntry>()

        fun get(path: String): CacheEntry {
            val cleanPath = CleanPath(path)
            if (!files.containsKey(cleanPath)) {
                files[cleanPath] = CacheEntry()
            }
            return files[cleanPath]!!
        }

        fun getOrNull(path: String): CacheEntry? {
            val cleanPath = CleanPath(path)
            return if (files.containsKey(cleanPath)) files[cleanPath] else null
        }

        fun remove(path: String) {
            files.remove(CleanPath(path))
        }
    }
}