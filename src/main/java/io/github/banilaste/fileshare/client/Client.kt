package io.github.banilaste.fileshare.client

import io.github.banilaste.duck.DuckProperties
import io.github.banilaste.duck.RemoteObject
import io.github.banilaste.duck.RemoteObjectIdentifier
import io.github.banilaste.duck.RemoteException
import io.github.banilaste.duck.generated.*
import io.github.banilaste.duck.util.LOGGER
import io.github.banilaste.duck.util.LogLevel
import io.github.banilaste.duck.util.getOrInput
import io.github.banilaste.duck.util.parseArgs
import io.github.banilaste.fileshare.client.caching.CachedFileManagerStub
import io.github.banilaste.fileshare.common.FaultyDatagramSocket
import io.github.banilaste.fileshare.common.FilesProperties
import java.lang.Exception
import java.net.InetAddress
import kotlin.system.exitProcess

fun main(args: Array<String>) {
    val parsedArgs = parseArgs(args)

    // Change log level
    LOGGER.level = LogLevel.valueOf(parsedArgs.getOrDefault("logLevel", "WARNING"))
    LOGGER.colorsEnabled = !parsedArgs.containsKey("noColors")

    // Init UDP server
    DuckProperties.socketFactory = { FaultyDatagramSocket(it, parsedArgs["failRate"]?.toDouble() ?: 0.0) }
    DuckProperties.localAddress = InetAddress.getByName(parsedArgs.getOrDefault("localIp", InetAddress.getLocalHost().hostAddress))
    DuckProject.init(
        parsedArgs.getOrDefault("localPort", "11111").toInt()
    )

    // Set cache timing
    FilesProperties.cacheTimeout = parsedArgs.getOrDefault("cachingTimeout", "10000").toLong()

    // Enable file caching
    RemoteObject.factories[FileManager::class] = {
        CachedFileManagerStub(
            it
        )
    }

    // Get address of remote file host
    val fileManager = retrieveFileManager(parsedArgs)

    print("> ")
    var command = readLine()

    while (command != null && !command.startsWith("exit")) {
        try {
            handleCommand(command, fileManager)
        } catch (e: CommandExecutionException) {
            LOGGER.error(e.message)
        } catch (e: RemoteException) {
            LOGGER.error(e.message)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        print("> ")
        command = readLine()
    }

    exitProcess(0)
}

/**
 * Ask for details about the file manager
 */
fun retrieveFileManager(args: Map<String, String>): FileManager {
    val serverIp = getOrInput("remoteIp", args, "remote ip address") {
        runCatching { InetAddress.getByName(it) }.getOrNull()
    }
    val serverPort =
        getOrInput(
            "remotePort",
            args,
            "remote port"
        ) { it.toIntOrNull() }

    return RemoteObject.stub(
        RemoteObjectIdentifier(0, serverIp, serverPort),
        FileManager::class
    )
}

fun handleCommand(command: String, fileManager: FileManager) {
    val parts = command.split(Regex("\\s+")).filter { it != "" }
    val commandName = if (parts.isEmpty()) ""  else parts[0]

    if (COMMANDS.containsKey(commandName)) {
        try {
            val result = COMMANDS[commandName]?.execute(parts, fileManager) ?: "Done"

            LOGGER.result(result)
        } catch (e: CommandExecutionException) {
            LOGGER.error(e.message)
            LOGGER.info(COMMANDS[commandName].toString().substring(3))
        }
    } else if (commandName != "") {
        LOGGER.info("command not recognized, available commands (exit for quitting) :\n${ COMMANDS.values.joinToString("\n") }")
    }

}