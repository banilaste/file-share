package io.github.banilaste.fileshare.server

import io.github.banilaste.duck.generated.FileListener
import io.github.banilaste.duck.generated.FileManager
import io.github.banilaste.duck.generated.Metadata
import io.github.banilaste.duck.generated.Modification
import io.github.banilaste.fileshare.common.CleanPath
import io.github.banilaste.fileshare.common.RandomFiles
import io.github.banilaste.fileshare.common.vigenere
import java.io.File
import java.nio.ByteBuffer
import java.nio.file.Files
import java.nio.file.InvalidPathException
import java.nio.file.NotDirectoryException
import java.util.*
import kotlin.collections.HashMap
import kotlin.concurrent.thread


class FileManagerImpl : FileManager() {
    // Watchers of the file
    private val watchers = HashMap<CleanPath, MutableList<FileListener>>()

    override fun read(path: String, offset: Long, size: Int): ByteArray {
        return RandomFiles.read(path, offset) {
            val fileLength = it.length()
            val length = if (size == -1) fileLength - offset else size.toLong()

            if (length + offset > fileLength || length < 0) {
                throw IndexOutOfBoundsException("file size exceeded")
            }

            length
        }
    }

    override fun write(path: String, offset: Long, content: ByteArray): Modification {
        val previouslyModified = kotlin.runCatching { this.modificationTime(path) }.getOrDefault(0)
        var result: ByteArray? = null

        // Notify for modification
        val cleanPath = CleanPath(path)

        RandomFiles.write(path, offset, content) {
            result = ByteArray(it.length().toInt())
            it.seek(0)
            it.read(result)
        }

        watchers[cleanPath]?.forEach {
            thread(start = true) {
                it.changed(cleanPath.toString(), result!!)
            }
        }

        return Modification(previouslyModified, modificationTime(path), result!!.size.toLong())
    }

    override fun crypt(path: String, key: String, reverse: Boolean): Modification {
        val previouslyModified = this.modificationTime(path)
        val cleanPath = CleanPath(path)
        val result = vigenere(RandomFiles.read(path, 0) { it.length() }, key, reverse)

        RandomFiles.write(
            path, 0,
            result
        )

        watchers[cleanPath]?.forEach {
            thread(start = true) {
                it.changed(cleanPath.toString(), result)
            }
        }

        return Modification(previouslyModified, modificationTime(path), result.size.toLong())
    }

    override fun insert(path: String, offset: Long, content: ByteArray): Modification {
        val previouslyModified = this.modificationTime(path)
        val cleanPath = CleanPath(path)
        var result: ByteArray? = null

        RandomFiles.open(cleanPath, "rw")
            .use {
                if (offset > it.length()) {
                    throw IndexOutOfBoundsException("offset exceed file size, please write with smaller offset")
                }

                // Read the following of the content
                val buffer = ByteArray((it.length() - offset).toInt())
                it.seek(offset)
                it.read(buffer)

                // Insert content
                it.seek(offset)
                it.write(content)

                // Write overwritten part
                it.seek(offset + content.size)
                it.write(buffer)

                // Read final content to update
                result = ByteArray(it.length().toInt())
                it.seek(0)
                it.read(result)
            }

        // Notify for modification
        watchers[cleanPath]?.forEach {
            thread(start = true) {
                it.changed(cleanPath.toString(), result!!)
            }
        }

        return Modification(previouslyModified, modificationTime(path), result!!.size.toLong())
    }

    override fun ls(path: String): List<String> {
        val result: Result<List<String>> = kotlin.runCatching {
            File(CleanPath(path).actualPath().toUri()).list()?.asList() ?: listOf()
        }

        if (result.isFailure) {
            throw NotDirectoryException(CleanPath(path).toString())
        }

        return result.getOrNull()!!
    }

    override fun delete(path: String) {
        val cleanPath = CleanPath(path)
        if (cleanPath.toString() == "/") {
            throw InvalidPathException(cleanPath.toString(), "cannot delete root directory")
        }

        // Notify for modification
        watchers[cleanPath]?.forEach {
            thread(start = true) {
                it.deleted(cleanPath.toString())
            }
        }

        Files.deleteIfExists(cleanPath.actualPath())
    }

    override fun watch(path: String, duration: Long, listener: FileListener) {
        val cleanPath = CleanPath(path)
        if (!watchers.containsKey(cleanPath)) {
            watchers[cleanPath] = LinkedList()
        }

        watchers[cleanPath]!!.add(listener)

        if (duration >= 0) {
            // Schedule stopping of watching
            thread(start = true) {
                Thread.sleep(duration)
                watchers[cleanPath]?.remove(listener)
                listener.expired(cleanPath.toString())
            }
        }
    }

    override fun metadata(path: String): Metadata {
        return Metadata(modificationTime = modificationTime(path), size = size(path))
    }

    private fun modificationTime(path: String): Long {
        return Files.getLastModifiedTime(
            CleanPath(
                path
            ).actualPath()
        ).toMillis()
    }

    fun size(path: String): Long {
        return Files.size(CleanPath(path).actualPath())
    }

    override fun mkdir(path: String) {
        Files.createDirectories(CleanPath(path).actualPath())
    }
}