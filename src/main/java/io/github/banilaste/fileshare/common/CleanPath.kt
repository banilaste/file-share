package io.github.banilaste.fileshare.common

import java.nio.file.InvalidPathException
import java.nio.file.Path
import java.nio.file.Paths
import java.util.ArrayDeque

/**
 * Wrapper for a cleaned path string, this class ensure the string inside is proper to
 * be used in the filesystem (one string per file)
 */
class CleanPath(path: String) {
    val cleanPathString: String

    init {
        val stack = ArrayDeque<String>()

        try {
            path.split(Regex("[/\\\\]")).forEach {
                when (it) {
                    ".." -> stack.pop()
                    "." -> { }
                    "" -> { }
                    else -> stack.push(it)
                }
            }
        } catch (e: NoSuchElementException) {
            throw InvalidPathException(path, "the path is invalid (going above root)")
        }

        cleanPathString = "/" + stack.reversed().joinToString("/")
    }

    fun actualPath(): Path {
        return Paths.get(FilesProperties.folder + cleanPathString)
    }

    override fun toString(): String {
        return cleanPathString
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CleanPath

        if (cleanPathString != other.cleanPathString) return false

        return true
    }

    override fun hashCode(): Int {
        return cleanPathString.hashCode()
    }


}