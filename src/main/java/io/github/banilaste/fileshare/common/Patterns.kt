package io.github.banilaste.fileshare.common

data class InputPattern(val pattern: Regex, val name: String, val optional: Boolean = false) {
    fun orEmpty(): InputPattern {
        return InputPattern(pattern, name, true)
    }
}

private val INT_PATTERN = "[0-9]+".toRegex()
private val ANY_PATTERN = ".+".toRegex()

val OFFSET = InputPattern(INT_PATTERN, "offset")
val SIZE = InputPattern(INT_PATTERN, "file size")
val DURATION = InputPattern(INT_PATTERN, "duration")
val FILE_NAME = InputPattern(ANY_PATTERN, "file path")
val CRYPT_KEY = InputPattern(ANY_PATTERN, "password")
