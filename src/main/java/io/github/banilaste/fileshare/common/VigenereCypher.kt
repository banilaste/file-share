package io.github.banilaste.fileshare.common

import java.nio.ByteBuffer

/**
 * Cypher the given byte array using vigenere cipher
 */
fun vigenere(buffer: ByteArray, key: String, reverse: Boolean): ByteArray {
    val sign = if (reverse) 1 else -1

    return buffer.mapIndexed { index: Int, byte: Byte -> (sign * key[index % key.length].toByte() + byte).toByte() }
            .toByteArray()
}