package io.github.banilaste.fileshare.common

import io.github.banilaste.duck.util.LOGGER
import java.net.DatagramPacket
import java.net.DatagramSocket

/**
 * Class emulating network troubles
 *
 * The socket fails to transmit some packet to the destination according to its
 * given rate
 */
class FaultyDatagramSocket(port: Int, private val failRate: Double = 0.0): DatagramSocket(port) {
    init {
        if (failRate != 0.0) {
            LOGGER.info("creating faulty datagram socket with fail rate $failRate")
        }
    }

    override fun send(p: DatagramPacket?) {
        if (failRate == 0.0 || failRate < Math.random()) {
            super.send(p)
        } else {
            LOGGER.debug("sending was randomly failed")
        }
    }
}