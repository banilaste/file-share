package io.github.banilaste.fileshare.common

import java.io.File
import java.io.FileNotFoundException
import java.io.RandomAccessFile
import java.nio.file.Files
import java.nio.file.Path

object RandomFiles {
    fun open(path: CleanPath, mode: String): RandomAccessFile = open(path.actualPath(), mode)
    fun open(path: Path, mode: String): RandomAccessFile = RandomAccessFile(path.toString(), mode)

    /**
     * Read part of file
     */
    fun read(path: String, offset: Long, fn: (RandomAccessFile) -> Long): ByteArray {
        val cleanPath = CleanPath(path)
        val realPath = cleanPath.actualPath()
        if (Files.isDirectory(realPath)) {
            throw NoSuchFileException(File(cleanPath.toString()), reason = "directory, not a file")
        } else if (!Files.exists(realPath)) {
            throw FileNotFoundException("$cleanPath not found")
        }

        return open(realPath, "r")
            .use {
                val bytes = ByteArray(fn(it).toInt())
                it.seek(offset)
                it.read(bytes)
                bytes
            }
    }

    /**
     * Write to file and return exact path
     */
    fun write(path: String, offset: Long, content: ByteArray, postAction: ((RandomAccessFile) -> Unit)? = null): Path {
        return CleanPath(path).actualPath().let {
            open(it, "rw")
                .use { file ->
                    if (offset > file.length()) {
                        throw IndexOutOfBoundsException("offset exceed file size, please write with smaller offset")
                    }

                    file.seek(offset)
                    file.write(content)

                    if (postAction != null) {
                        postAction(file)
                    }
                }

            it
        }

    }


}